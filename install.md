1. Enable developer mode (see here for instructions http://www.cnet.com/how-to/how-to-enable-developer-mode-on-a-chromebook/)
2. Run `wget -q -O - https://raw.github.com/skycocker/chromebrew/master/install.sh | bash` to install chromebrew 
see https://github.com/skycocker/chromebrew
3. `sudo -i` to root
4. Edit /usr/local/bin/crew and comment out the line `abort "Chromebrew should not be run as root." if Process.uid == 0 && @command != "remove"`
this will allow you to run `crew install` as root
5. Run `crew install vim` and wait for the install to finish. You should now be able to run Vim with the command `vim`