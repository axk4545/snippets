1. Download the appropriate netinstall image from http://download.opensuse.org/distribution/$VERSION/iso/
2. Mount the iso and extract the initrd and linux image. Their locations can be found in /boot/grub/grub.cfg inside the iso.
3. Copy the initrd and linux image to the location of and existing initrd and vmlinuz on the target system. 
4. Rename the files copied from the iso to the ones already in /boot so that you don't have to edit the grub menu to make it work(kexec would also work if your system has it. see man kexec for more info)
5. Boot the system from the entry whose files you changed above and when it fails to find media select no and drop to a shell
6. configure the network and choose HTTP as install source. Set the server to download.opensuse.org and the directory to /distribution/$VERSION/repo/oss/
7. Proceed with the install

https://en.opensuse.org/SDB:Network_installation was used a reference for writing this.
The procedure would be generalizeable to any netinstall disk for any distribution.