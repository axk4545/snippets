1. Download calendar from TigerCenter.
2. Login to RIT Gmail and go to calendar.
3. In settings, choose Calendars.
4. Click import calendar and choose downloaded .ics file.
5. Choose a destination calendar.
6. Finish import and adjust events as necessary to correct recurrence and timezone.