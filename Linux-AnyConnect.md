1. Install `openconnect` and `vpnc` based on your distribution.
2. Run `openconnect -b -s /etc/vpnc/vpnc-script [url of VPN gateway]` as root
3. Accept the certificate.
4. Enter your username and password when prompted.
5. OpenConnect will then continue to run and once the connection succeeds it will fork itself to the background printing your IP, the device associated with the IP, and its PID.